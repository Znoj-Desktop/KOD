### **Description**
My text compression algorithm  
  
- reading input file and creating matrix
- saving matrix into file 'matrix'
  
---
- restoring matrix from the file
- creating output for printing coded message
- creating file 'fullyCoded' for printing coded message in chars for smaller size  
  
---
- opening file 'fullyCoded' for getting coded message in chars
- creating file 'input2' for printing decoded message - for control  
  
---
### **Technology**
C++

---
### **Year**
2015

---
### **Screenshot**
#### Compressed books:  
![compare_size](README/compare_size.png)
![compare_size_hp7](README/compare_size_hp7.png)
![compare_size_pycha_a_predsudek](README/compare_size_pycha_a_predsudek.png)
  
#### Worst case:  
![compare_size_worst_case](README/compare_size_worst_case.png)
  
#### Creating matrix:  
![l_bin](README/l_bin.png)
![l_input](README/l_input.png)
![l_input_proporcion](README/l_input_proporcion.png)
![l_matrix](README/l_matrix.png)
  
#### Real output:  
##### Matrix:  
![matrix](README/matrix.png)  
##### Output:  
![output](README/output.png)  
  
#### Output log:  
![program_run](README/program_run.png)
