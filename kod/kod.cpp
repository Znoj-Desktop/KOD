#define _CRT_SECURE_NO_DEPRECATE
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
//kvuli sqrt() a ceil - zaokrouhlovani
#include "math.h"

//maximalni pocet slov v poli
const int max_length = 156000;
const int max_length_dividers = 5000;
//maximalni delka slova
const int max_word = 255;
//maximalni delka zakodovaneho znaku
const int max_bin_length = 31;

char words[max_length][max_word];
int w_counter = 0;
int w_width = 0;
int w_height = 0;

char dividers[max_length][max_word];
int d_counter = 0;
int d_width = 0;
int d_height = 0;

//promenne pro vypocet binarniho kodu
int word_w_bin_characters = 0;
int word_h_bin_characters = 0;

int divider_w_bin_characters = 0;
int divider_h_bin_characters = 0;

char bin_word[max_bin_length + 1];
int index_bin_length = 0;
char bin_word_correct_length[max_bin_length];


//z binarniho cisla vraci dekadicke
int convert_from_binary(int n){
	int pom = 0;
	int result = 0;
	int base = 1;
	while (n > 0){
		pom = n % 10;
		result = result + pom * base;
		base *= 2;
		n /= 10;
	}
	return result;
}

//rekurzivni alg pro konverzaci z desitkove soustavy do binarni
void convert_to_binary_alg(int n){
	if (n / 2 != 0) {
		convert_to_binary_alg(n / 2);
	}
	bin_word[index_bin_length++] = '0' + (n % 2);
	//printf("%d", n % 2);
}


char *convert_to_binary(int n, int length = 0, bool isWord = true, bool isWidth = true){
	convert_to_binary_alg(n);
	if (length != 0){
		for (int i = 0; i < length - index_bin_length; i++){
			bin_word_correct_length[i] = '0';
		}
		for (int i = length - index_bin_length; i < length; i++){
			bin_word_correct_length[i] = bin_word[i - (length - index_bin_length)];
		}
		bin_word_correct_length[length] = '\0';
		index_bin_length = 0;
		//printf("%s", bin_word);
		return bin_word_correct_length;
	}
	else{
		bin_word[index_bin_length] = '\0';
		index_bin_length = 0;
		//printf("%s", bin_word);
		return bin_word;
	}
}


//alg pro vyhledavani v poli (bud slov nebo oddelovacu) s tim, ze je nesetrideny
bool exists(char *word, int length, char field[][max_word], int counter){
	bool diff = false;

	for (int n = 0; n < counter; n++){
		diff = false;
		for (int i = 0; i < length; i++){
			if (field[n][i] != word[i]){
				diff = true;
			}
		}
		//nove slovo je obsazeno v puvodnim, ale je kratsi
		if (field[n][length] != '\0'){
			diff = true;
		}
		if (!diff){
			return true;
		}
	}
	return false;
}


int get_word_index(char *word, int length, char field[][max_word], int counter){
	bool diff = false;

	for (int n = 0; n < counter; n++){
		diff = false;
		for (int i = 0; i < length; i++){
			if (field[n][i] != word[i]){
				diff = true;
			}
		}
		//tohle neni presne to hledane slovo
		if (field[n][length] != '\0'){
			diff = true;
		}
		if (!diff){
			return n;
		}
	}
	//tohle by nastat nemelo
	printf("ERROR - word (%s) (%d chars) does not exist - function get_word_index\n", word, length);
	return 0;
}

//je cislo, nebo pismeno
bool is_word(int character){
	//vsechno z rozsirene tabulky je slovo... jakoze diakritika a tak
	if (character > 127){
		return true;
	}
	//97 == a; 122 == z
	if (character > 96){
		if (character < 123){
			return true;
		}
		return false;
	}
	//65 == A; 90 == Z
	if (character > 64){
		if (character < 91){
			return true;
		}
		return false;
	}
	//48 == 0; 57 == 9
	if (character > 47 && character < 58){
		return true;
	}
	return false;
}

//TODO overit
char *transform(bool isWord, int index){
	int w;
	int h;
	int w_bin;
	int h_bin;
	if (isWord){
		w = w_width;
		h = w_height;
		w_bin = word_w_bin_characters;
		h_bin = word_h_bin_characters;
	}
	else{
		w = d_width;
		h = d_height;
		w_bin = divider_w_bin_characters;
		h_bin = divider_h_bin_characters;
	}

	int row = index / w;
	int column = index % w;

	//printf("index: %d, w: %d, h: %d\n", index, column, row);

	char *bin_ptr;
	char bin_row[max_bin_length];
	char bin_column[max_bin_length];
	bin_ptr = convert_to_binary(row, h_bin, isWord, false);
	strcpy_s(bin_row, bin_ptr);
	bin_ptr = convert_to_binary(column, w_bin, isWord, true);
	strcpy_s(bin_column, bin_ptr);
	//printf("binary: %d, w: %s, h: %s\n", index, bin_column, bin_row);

	char *result_code = (char *) malloc(2 * max_bin_length);
	strcpy(result_code, bin_row);
	strcat(result_code, bin_column);
	//printf("binary: %s\n", result_code);
	return result_code;
}

char *get_bin(bool isWord, char *word, int length){
	char *field;
	int word_index = 0;
	if (isWord){
		word_index = get_word_index(word, length, words, w_counter);
		if (word_index >= 0){
			return transform(true, word_index);
		}
		else{
			//tohle by nemelo nastat
			printf("ERROR - reading word (%s) (%d chars) was not found in dictionary\n", word, length);
		}
		field = words[w_counter++];
	}
	else{
		word_index = get_word_index(word, length, dividers, d_counter);
		if (word_index >= 0){
			return transform(false, word_index);
		}
		else{
			//tohle by nemelo nastat
			printf("ERROR - reading word (%s) (%d chars) was not found in dictionary\n", word, length);
		}
		field = dividers[d_counter++];
	}
	for (int i = 0; i < length; i++){
		field[i] = word[i];
	}
	return NULL;
}

void save(bool isWord, char *word, int length){
	char *field;
	if (isWord){
		if (exists(word, length, words, w_counter)){
			return;
		}
		field = words[w_counter++];
	}
	else{
		if (exists(word, length, dividers, d_counter)){
			return;
		}
		field = dividers[d_counter++];
	}
	for (int i = 0; i < length; i++){
		field[i] = word[i];
	}
}



//odmocnina z poctu slov zaokrouhlena nahoru na nejblizsi vyssi cele cislo udava sirku
//celkovy pocet slov vydeleny sirkou zaokrouhleny na nejblizsi vyssi cele cislo udava vysku matice
//matice je tedy priblizne ctvercova
void count_dimensions_sqrt_alg(int counter, int &width, int &height){
	double sqrt_pom = sqrt((double)counter);
	width = (int)ceil(sqrt_pom);
	sqrt_pom = counter / (double)width;
	height = (int)ceil(sqrt_pom);

	/*
	printf("width: %d\n", width);
	printf("width binary: %s\n", convert_to_binary(width));
	printf("height: %d\n", width);
	printf("height binary: %s\n", convert_to_binary(height));
	printf("control sum w*h: %d\n\n", width*height);
	*/
}

void count_dimensions_sqrt(){
	//words
	//printf("words: %d\n", w_counter);
	count_dimensions_sqrt_alg(w_counter, w_width, w_height);

	//dividers
	//printf("dividers: %d\n", d_counter);
	count_dimensions_sqrt_alg(d_counter, d_width, d_height);
}

//type 0 = odmocnina z celkoveho poctu = ctvercova matice
void count_dimensions(int type){
	if (type == 0){
		count_dimensions_sqrt();
		return;
	}
}

void create_matrix(FILE *pFile){
	int character = fgetc(pFile);
	char word[max_word];
	char word_length = 0;
	bool reading_word = false;
	bool reading_divider = false;
	while (character != EOF){
		//znak
		if (is_word(character)){
			if (reading_divider){
				save(0, word, word_length);
				word_length = 0;
				reading_divider = false;
			}
			word[word_length++] = character;
			reading_word = true;
		}
		//oddelovac
		else{
			if (reading_word){
				save(1, word, word_length);
				word_length = 0;
				reading_word = false;
			}
			word[word_length++] = character;
			reading_divider = true;
		}
		character = fgetc(pFile);
	}

	//je potreba jeste ulozit to co tam zbylo...
	if (reading_divider){
		save(0, word, word_length);
		word_length = 0;
		reading_divider = false;
	}
	else{
		save(1, word, word_length);
		word_length = 0;
		reading_word = false;
	}

	count_dimensions(0);
}

void print_all(){
	printf("Words:\n");
	for (int i = 0; i < w_counter - 1; i++){
		printf("%s, ", words[i]);
	}
	printf("%s\n", words[w_counter - 1]);

	printf("\nDividers:\n");
	for (int i = 0; i < d_counter - 1; i++){
		printf("|%s| ", dividers[i]);
	}
	printf("|%s|\n", dividers[d_counter - 1]);
}

//pred kazdy znak ',' a '\' vypise na vypis znak '\'
void print_with_escape(FILE *pFile, bool printEscape, char *field){
	int index = 0;
	while (field[index] != '\0'){
		if (field[index] == ',' || field[index] == '\\'){
			if (printEscape)
				fprintf(pFile, "%c%c", '\\', field[index]);
			else
				fprintf(pFile, "%c", field[index]);
		}
		else{
			fprintf(pFile, "%c", field[index]);
		}
		index++;
	}
	fprintf(pFile, "%c", ',');
}

void print_matrix_alg(FILE *pFile, bool index, int width, char field[][max_word], int field_length){
	int row_counter = 1;
	//vypis cisla sloupcu
	if (index){
		for (int i = 0; i < width; i++){
			fprintf(pFile, "   %d  ", i);
		}
		fprintf(pFile, "\n");
	}

	//prvni (nulty) prvek vypisu mimo cyklus a vypisu index prvniho radku bez nutnosti podminky v kazdym cyklu
	if (index){
		fprintf(pFile, "%d ", row_counter - 1);
		row_counter++;
	}
	print_with_escape(pFile, !index, field[0]);
	//vypis jednotlivych radku
	for (int i = 1; i < field_length; i++){
		if (i%width == 0){
			if (index){
				fprintf(pFile, "\n%d ", row_counter - 1);
				row_counter++;
			}
		}
		print_with_escape(pFile, !index, field[i]);
	}
	fprintf(pFile, "\n");
}

//index = vypisovani indexu sloupcu a radku matice
void print_matrix(FILE *pFile, bool index = false){

	//VYPIS SLOV
	//printf("Words:\n");
	fprintf(pFile, "%d %d\n", w_width, w_counter);
	print_matrix_alg(pFile, index, w_width, words, w_counter);

	//VYPIS ODDELOVACU SLOV
	//printf("\nDividers:\n");
	fprintf(pFile, "%d %d\n", d_width, d_counter);
	print_matrix_alg(pFile, index, d_width, dividers, d_counter);

}

void load_matrix(int width, int words, FILE *pFile, bool isWord){
	char word[max_word];
	char word_length = 0;
	//preskoceni odradkovani za velikosti matice
	int character = fgetc(pFile);
	while (words > 0){
		character = fgetc(pFile);
		//znak
		if (character != ','){
			//carka neni oddelovac, ale znak...
			if (character == '\\'){
				int pom = character;
				character = fgetc(pFile);
				//pokud neni dvojice '\,' pak se zapise cela, jinak se zapise jen ','
				if (character != ',' && character != '\\'){
					word[word_length++] = pom;
				}
			}
			word[word_length++] = character;
		}
		//oddelovac
		//kazdy vstup konci carkou, takze vsechna slova budou vzdy ulozena
		else{
			save(isWord, word, word_length);
			words--;
			if (words % 100 == 0){
				word_length = 0;
			}
			word_length = 0;
		}
	}
}

void load_matrix_from_file(FILE *pFile){
	//nacitani vstupu
	int w_from_file = 0;
	int h_from_file = 0;
	
	//matice slov
	if (fscanf_s(pFile, "%i %i", &w_from_file, &h_from_file) <= 0){
		printf("INPUT ERROR - there is no matrix dimensions as should be...\n");
	}
	load_matrix(w_from_file, h_from_file, pFile, true);

	//matice oddelovacu
	if (fscanf_s(pFile, "%i %i", &w_from_file, &h_from_file) <= 0){
		printf("INPUT ERROR - there is no matrix dimensions as should be...\n");
	}
	load_matrix(w_from_file, h_from_file, pFile, false);

	count_dimensions(0);
}

void set_length_of_codes(){
	//slova
	//vezmu maximalni mozny index a dle nej zjistim delku binarniho kodu
	convert_to_binary_alg(w_width);
	word_w_bin_characters = index_bin_length;
	index_bin_length = 0;
	convert_to_binary_alg(w_height);
	word_h_bin_characters = index_bin_length;
	index_bin_length = 0;

	//oddelovace
	convert_to_binary_alg(d_width);
	divider_w_bin_characters = index_bin_length;
	index_bin_length = 0;
	convert_to_binary_alg(d_height);
	divider_h_bin_characters = index_bin_length;
	index_bin_length = 0;
}

void code_message(FILE *input, FILE *output){
	int character = getc(input);
	char word[max_word];
	char word_length = 0;
	bool reading_word = false;
	bool reading_divider = false;

	set_length_of_codes();

	char *out_code;
	while (character != EOF){
		//znak
		if (is_word(character)){
			if (reading_divider){
				out_code = get_bin(0, word, word_length);
				word[word_length] = '\0';
				//ladici vypis
				//printf("divider: %s, code: %s\n", word, out_code);
				fprintf(output, "%s", out_code);
				free(out_code); //pamet alokovana pomoci malloc se musi uvolnit...
				word_length = 0;
				reading_divider = false;
			}
			word[word_length++] = character;
			reading_word = true;
		}
		//oddelovac
		else{
			if (reading_word){
				out_code = get_bin(1, word, word_length);
				word[word_length] = '\0';
				//ladici vypis
				//printf("word: %s, code: %s\n", word, out_code);
				fprintf(output, "%s", out_code);
				free(out_code); //pamet alokovana pomoci malloc se musi uvolnit...
				word_length = 0;
				reading_word = false;
			}
			word[word_length++] = character;
			reading_divider = true;
		}
		character = getc(input);
	}

	//je potreba jeste ulozit to co tam zbylo...
	if (reading_divider){
		out_code = get_bin(0, word, word_length);
		word[word_length] = '\0';
		//ladici vypis
		//printf("divider: %s, code: 0 + %s\n", word, out_code);
		fprintf(output, "%s", out_code);
		free(out_code); //pamet alokovana pomoci malloc se musi uvolnit...
		word_length = 0;
		reading_divider = false;
	}
	else{
		out_code = get_bin(1, word, word_length);
		word[word_length] = '\0';
		//ladici vypis
		//printf("word: %s, code: 1 + %s\n", word, out_code);
		fprintf(output, "%s", out_code);
		free(out_code); //pamet alokovana pomoci malloc se musi uvolnit...
		word_length = 0;
		reading_word = false;
	}
}

//predpokladam, ze zprava zacina slovem
//TODO - prvni znak urci, cim zacina zprava
//strida se vzdy slovo s oddelovacem
//binarni cislo je ve tvaru bin_word.bin_column
void decode_message(FILE *fileIn, FILE *fileOut){
	set_length_of_codes();
	char word[max_word];
	char word_length = 0;
	bool reading_word = true;
	int w;
	int h;
	int index;
	while (true){
		if (reading_word){
			if (fgets(word, word_w_bin_characters + 1, fileIn) == NULL){
				return;
			}
			h = convert_from_binary(atoi(word));

			if (fgets(word, word_h_bin_characters + 1, fileIn) == NULL){
				printf("Chyba vstupu pri cteni slova - funkce decode_message()");
				return;
			}

			w = convert_from_binary(atoi(word));

			index = h*w_width + w;
			reading_word = false;
			/*
			printf("d: %d - ", h);
			printf("%d -> ", w);
			printf("index: %d\n", index);
			*/
			fprintf(fileOut, "%s", words[index]);

		}
		else{
			if (fgets(word, divider_h_bin_characters + 1, fileIn) == NULL){
				return;
			}
			h = convert_from_binary(atoi(word));

			if (fgets(word, divider_w_bin_characters + 1, fileIn) == NULL){
				printf("Chyba vstupu pri cteni oddelovace - funkce decode_message()");
				return;
			}

			w = convert_from_binary(atoi(word));

			index = h*d_width + w;
			reading_word = true;
			/*
			printf("d: %d - ", h);
			printf("%d -> ", w);
			printf("index: %d\n", index);
			*/
			fprintf(fileOut, "%s", dividers[index]);

		}
		
	}
}


//prevod z binarniho kodu zapsaneho v textovem souboru do znaku pro mensi velikost
void bin_to_char(FILE *input, FILE *output){
	int character = getc(input);
	char word[8];
	int index = 0;

	while (character != EOF){
		word[index++] = character;
		if (index == 8){
			int x = convert_from_binary(atoi(word));
			fprintf(output, "%c", x);
			index = 0;
		}
		character = getc(input);
	}

	//to co neni nasobkem osmi...
	fprintf(output, "%c", convert_from_binary(atoi(word)));
	//posledni cislo v souboru udava, kolik cisel se ma z predchoziho pouzit
	fprintf(output, "%c", index);
	
}

void char_to_bin(FILE *input, FILE *output){
	int character = getc(input);

	int number1 = -1;
	int number2 = 0;
	bool first = true;
	while (character != EOF){
		if (first){
			if (number1 == -1)
				number1 = character;
			else{
				number2 = character;
				first = false;
			}
		}
		else{
			number1 = number2;
			number2 = character;
		}

		character = getc(input);
		//posledni dva znaky jsou ulozeny tak, ze posledni znak urcuje delku predposledniho
		if (character == EOF){
			char * pom = convert_to_binary(number1, 8);
			pom[number2] = '\0';
			fprintf(output, "%s", pom);
		}
		else{
			if (!first){
				fprintf(output, "%s", convert_to_binary(number1, 8));
			}
		}
	}
}


int main(){
	FILE * fileInput;
	FILE * fileInput2;
	FILE * fileMatrix;
	FILE * fileMatrix2;
	FILE * fileOutput;
	FILE * fileOutput2;
	FILE * fileFullyCoded;

	//----------------------------------vytvoreni slovniku


	
	printf("Reading file 'input' and creating matrix\n");
	fileInput = fopen("input", "r");
	if (fileInput == NULL){
		perror("Error reading file 'input'. Standard input will be used...");
		fileInput = stdin;

	}

	create_matrix(fileInput);
	
	if (fileInput != NULL){
		fclose(fileInput);
	}

	printf("Saving matrix into file 'matrix'\n");
	fileMatrix = fopen("matrix", "w");
	if (fileMatrix == NULL){
		perror("Error creating file 'matrix'. Standard output will be used...");
		fileMatrix = stdout;

	}
	
	print_matrix(fileMatrix);
	
	if (fileMatrix != NULL){
		fclose(fileMatrix);
	}
	


	//----------------------------------nahrati slovniku


	
	printf("Loading matrix from file 'matrix' and creating matrix\n");
	fileMatrix = fopen("matrix", "r");
	if (fileMatrix == NULL){
		perror("Error opening file 'matrix'. Standard input will be used...");
		fileMatrix = stdin;

	}

	load_matrix_from_file(fileMatrix);
	
	if (fileMatrix != NULL){
		fclose(fileMatrix);
	}
	
	
	
	printf("Saving matrix into file 'matrix2' - for controll\n");
	fileMatrix2 = fopen("matrix2", "w");
	if (fileMatrix2 == NULL){
		perror("Error creating file 'matrix2'. Standard output will be used...");
		fileMatrix2 = stdout;

	}
	
	print_matrix(fileMatrix2);
	
	if (fileMatrix2 != NULL){
		fclose(fileMatrix2);
	}
	


	//----------------------------------komprimace

	
	printf("Opening file 'input' for coding message\n");
	fileInput = fopen("input", "r");
	if (fileInput == NULL){
		perror("Error reading file 'input'. Standard input will be used...");
		fileInput = stdin;

	}

	printf("Creating file 'output' for printing coded message\n");
	fileOutput = fopen("output", "w");
	if (fileOutput == NULL){
		perror("Error creating file 'output'. Standard output will be used...");
		fileOutput = stdout;
	}

	code_message(fileInput, fileOutput);

	if (fileInput != NULL){
		fclose(fileInput);
	}
	if (fileOutput != NULL){
		fclose(fileOutput);
	}


	fileOutput = fopen("output", "r");
	if (fileOutput == NULL){
		perror("Error opening file 'output'. Standard input will be used...");
		fileOutput = stdin;
	}

	printf("Creating file 'fullyCoded' for printing coded message in chars for smaller size.\n");
	fileFullyCoded = fopen("fullyCoded", "wb");
	if (fileFullyCoded == NULL){
		perror("Error creating file 'fullyCoded'. Standard output will be used...");
		fileFullyCoded = stdout;
	}

	bin_to_char(fileOutput, fileFullyCoded);

	if (fileOutput != NULL){
		fclose(fileOutput);
	}
	
	if (fileFullyCoded != NULL){
		fclose(fileFullyCoded);
	}


	//----------------------------------dekomprimace


	
	printf("Opening file 'fullyCoded' for getting coded message in chars.\n");
	//je treba cist binarne kvuli cislu ASCII 26 - coz je ve windows ctrl+z a znaci to konec vstupu v cmd...
	fileFullyCoded = fopen("fullyCoded", "rb");
	if (fileFullyCoded == NULL){
		perror("Error opening file 'fullyCoded'. Standard input will be used...");
		fileFullyCoded = stdin;
	}

	printf("Creating file 'output2' for printing coded message from binary file\n");
	fileOutput2 = fopen("output2", "w");
	if (fileOutput2 == NULL){
		perror("Error creating file 'output2'. Standard output will be used...");
		fileOutput2 = stdout;
	}

	char_to_bin(fileFullyCoded, fileOutput2);

	if (fileOutput2 != NULL){
		fclose(fileOutput2);
	}

	if (fileFullyCoded != NULL){
		fclose(fileFullyCoded);
	}
	
	printf("Opening file 'output' for decoding message\n");
	fileOutput = fopen("output2", "r");
	if (fileOutput == NULL){
		perror("Error reading file 'output'. Standard input will be used...");
		fileOutput = stdin;
	}

	printf("Creating file 'input2' for printing decoded message - for controll\n");
	fileInput2 = fopen("input2", "w");
	if (fileInput2 == NULL){
		perror("Error creating file 'input2'. Standard output will be used...");
		fileInput2 = stdout;

	}

	decode_message(fileOutput, fileInput2);
	
	if (fileOutput != NULL){
		fclose(fileOutput);
	}
	if (fileInput2 != NULL){
		fclose(fileInput2);
	}
	
	return 0;
}